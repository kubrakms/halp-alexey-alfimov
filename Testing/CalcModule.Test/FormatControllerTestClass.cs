﻿
namespace SoftServe.Halp.Testing.CalcModule.Test
{
    using System.IO;  
    using System.Collections.Generic;


    using NUnit.Framework;


    using SoftServe.Halp.Modules.CalcModule;
    using SoftServe.Halp.Modules.CalcModule.Models;  
    using SoftServe.Halp.Modules.CalcModule.Proxies;   
    using SoftServe.Halp.Modules.CalcModule.Abstraction;

    [TestFixture]
    public partial class FormatControllerTestClass
    {
        
        [SetUp]
        public void Initalize()
        {
            var assemblyPath = typeof(FormatControllerTestClass).Assembly.Location;
            RootDataPath = Path.Combine(Path.GetDirectoryName(assemblyPath), "Data");
        }


        [Test(Description = "Examines ReadFormat functionality"), TestCaseSource("ReadFormatActionData")]
        public void ReadFormat_ActionTest(string file, eFormatType type, HalpFormatData etalone)
        {
            var fileExt = getExtByFormatType(type);

            Assert.IsFalse(string.IsNullOrEmpty(fileExt));

            var filePath = string.Format("{0}\\{3}\\{1}.{2}",
                new object[] { RootDataPath, file, fileExt, "ReadFormat" });

            Assert.IsTrue(File.Exists(filePath));

            using (var controller = LogicContainer.Instance.GetService<IFormatContract>())
            {
                var actual = default(HalpFormatData);
                var cmp = new Comparers.HalpModelComparer();
                Assert.IsTrue(cmp.Compare(etalone, actual) == 0);
            }
        }


        [Test(Description = "Examines WriteFormat functionality"), TestCaseSource("WriteFormatActionData")]
        public void WriteFormat_Action(eFormatType type, IEnumerable<HalpFormatData> etalone, string md5)
        {

        }


        [Test(Description = "Examines DetectFormatTypeAtExt functionality"), TestCaseSource("DetectFormatTypeAtExtData")]
        public void DetectFormatTypeAtExt_Action(eFormatType etalone, string sourceFile)
        {
            using (var controller = LogicContainer.Instance.GetService<IFormatContract>())
            {
                var expected = controller.DetectFormatTypeAtExt(sourceFile);

                Assert.AreEqual(expected, etalone);
            }
        }

        [Test(Description = "Examines controller initialization & utilizatin functionality")]
        public void ControllerUtilization() 
        {
            using (var controller = LogicContainer.Instance.GetService<IFormatContract>())
            {

            }
        }
    }
}
