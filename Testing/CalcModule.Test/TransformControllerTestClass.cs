﻿
namespace SoftServe.Halp.Testing.CalcModule.Test
{
    using System;
    using System.IO;
    using NUnit.Framework;
    using SoftServe.Halp.Modules.CalcModule;
    using SoftServe.Halp.Modules.CalcModule.Abstraction;
    using SoftServe.Halp.Modules.CalcModule.Models;


    [TestFixture]
    public partial class TransformControllerTestClass
    {
        [SetUp]
        public void Initalize()
        {
            var assemblyPath = typeof(FormatControllerTestClass).Assembly.Location;
            RootDataPath = Path.Combine(Path.GetDirectoryName(assemblyPath), "Data");
        }

        [Test(Description = "Examines ComputeTriangle functionality"), TestCaseSource("ComputeTriangleData")]
        public void ComputeTriangle_Action(HalpFormatData original, HalpFormatData expected)
        {
            Assert.IsNotNull(original);
            Assert.IsNotNull(expected);

            using (var controller = LogicContainer.Instance.GetService<ITransformContract>())
            {
                var actual = default(HalpFormatData);
                controller.ComputeTriangle(original, out actual);

                var cmp = new Comparers.HalpModelComparer();
                Assert.IsTrue(cmp.Compare(expected, actual) == 0);
            }
        }

        [Test(Description = "Examines controoler creation & utilization")]
        public void ControllerUtilization()
        {
            using (var controller = LogicContainer.Instance.GetService<ITransformContract>())
            {
                Assert.IsNotNull(controller); 
            }
        }
    }
}
