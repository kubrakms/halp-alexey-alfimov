﻿/// <copyright file="TransformControllers.cs" company="SoftServe">
/// Copyright (c) 2013 All Rights Reserved
/// </copyright>
/// <author>Kucher Inna</author>


namespace SoftServe.Halp.Modules.CalcModule.Controllers
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    internal class TransformController : Abstraction.ITransformContract, IDisposable
    {
        
        #region ITransformContract implementation
        public void ComputeTriangle(Models.HalpFormatData oldTriang, out Models.HalpFormatData newTriang)
        {
            throw new NotImplementedException();
        }

        public void ComputeTriangleBath(IEnumerable<Models.HalpFormatData> oldTriangSet, out IEnumerable<Models.HalpFormatData> newTriangSet)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
