﻿// <copyright file="FormatController.cs" company="SoftServe">
// Copyright (c) 2013 All Rights Reserved
// </copyright>
// <author>Kucher Inna</author>
// <date>2/19/2013 8:11:49 PM</date>
// <summary>TODO: Update summary</summary>

namespace SoftServe.Halp.Modules.CalcModule.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using SoftServe.Halp.Modules.CalcModule.Abstraction;
    using SoftServe.Halp.Modules.CalcModule.Models;

    /// <summary>
    /// TODO: Update summary
    /// </summary>
    internal class FormatController : IFormatContract, IDisposable
    {
    
        #region IFormatContract implementation

        public IEnumerable<HalpFormatData> ReadFormat(string sourceFile, Proxies.eFormatType typeSource)
        {
            throw new NotImplementedException();
        }

        public void WriteFormat(IEnumerable<HalpFormatData> data, Proxies.eFormatType typeDest, string destFile)
        {
            throw new NotImplementedException();
        }

        public Proxies.eFormatType DetectFormatTypeAtExt(string sourceFile)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region IDisposable implementation

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}
