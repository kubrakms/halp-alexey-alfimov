﻿// -----------------------------------------------------------------------
// <copyright file="TransformMoq.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Proxies
{
    using System;
    using System.IO;   
    
    using SoftServe.Halp.Modules.CalcModule.Abstraction;

    using Moq;


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public static class Moqs
    {
        static Moqs()
        {
            try
            {
                Initalize();
            }
            catch (Exception)
            {
                /// TODO: Add log audit logic here
            }
        }

        public static ITransformContract TransformProxyMoq { get { return mTransformProxyMoq.Object; } }
        public static IFormatContract FormatProxyMoq { get { return mFormatProxyMoq.Object; } }

        private static void Initalize()
        {
            lock (__mutex)
            {
                initializeFormat();
                initializeTransform();
            }
        }

        private static void initializeTransform()
        {
            string input = "inputTransform.csv";
            //mTransformProxyMoq.Setup(x => x.WriteToFile(input, "output.csv")).Returns(() => (File.Exists(input) ? 0 : -21312));
        }

        private static void initializeFormat()
        {
            string input = "inputFormat.csv";
            //mFormatProxyMoq.Setup(x => x.FormatAndWriteToFile(input, "output.csv")).Returns(() => (File.Exists(input) ? 0 : -21312));
        }

        private static readonly object __mutex = new object();
        private static readonly Mock<IFormatContract> mFormatProxyMoq = new Mock<IFormatContract>(MockBehavior.Loose);
        private static readonly Mock<ITransformContract> mTransformProxyMoq = new Mock<ITransformContract>(MockBehavior.Loose);
    }
}
