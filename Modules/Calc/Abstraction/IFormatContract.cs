﻿// -----------------------------------------------------------------------
// <copyright file="IFormatContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Abstraction
{
    using System;
    using System.Collections.Generic;

    using SoftServe.Halp.Modules.CalcModule.Models;
    using SoftServe.Halp.Modules.CalcModule.Proxies;

    /// <summary>
    /// Represents fpFormat utility behavior
    /// </summary>
    public interface IFormatContract : IDisposable
    {
        /// <summary>
        /// Reads format
        /// </summary>
        /// <param name="sourceFile">Source file path</param>
        /// <param name="typeSource">Source file format</param>
        /// <returns>Object collection</returns>
        /// <exception cref="ArgumentException">When sourceFile does not exists</exception>
        /// <exception cref="NotSupportedException">When typeFormat does not consistent with file format</exception>
        IEnumerable<HalpFormatData> ReadFormat(string sourceFile, eFormatType typeSource);


        /// <summary>
        /// Writes file with specified format
        /// </summary>
        /// <param name="data">Data collection</param>
        /// <param name="typeDest">destinated data type</param>
        /// <param name="destFile">file destination</param>
        /// <exception cref="ArgumentException">When data is null</exception>
        /// <exception cref="InvalidOperationException">When method can't create file of destFile</exception>
        void WriteFormat(IEnumerable<Models.HalpFormatData> data, eFormatType typeDest, string destFile);

        /// <summary>
        /// Detects file format
        /// </summary>
        /// <param name="sourceFile">Full file path</param>
        /// <returns>Type format</returns>
        /// <exception cref="ArgumentException">When source file not found</exception>
        /// <exception cref="InvalidOperationException">When file format was not detected</exception>
        eFormatType DetectFormatTypeAtExt(string sourceFile);
    }
}
