﻿// -----------------------------------------------------------------------
// <copyright file="ITransformContract.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Abstraction
{
    using System;
    using System.Collections.Generic;
    using SoftServe.Halp.Modules.CalcModule.Models;

    /// <summary>
    /// Represents fpTransform utility behavior
    /// </summary>
    public interface ITransformContract : IDisposable
    {
        /// <summary>
        /// omputes triangle coordinates accourding new base point
        /// </summary>
        /// <param name="oldTriang">Old triangle</param>
        /// <param name="newTriang">New triangle</param>
        /// <exception cref="ArgumentException">If old triangle has not Id or no Name</exception>
        /// <exception cref="ArgumentNullException">If old triangle is Null</exception>
        void ComputeTriangle(HalpFormatData oldTriang, out HalpFormatData newTriang);

        /// <summary>
        /// Computes triangle coordinates accourding new base point in batch mode
        /// </summary>
        /// <param name="oldTriang">Old triangle set</param>
        /// <param name="newTriang">New triangle set</param>
        /// <exception cref="ArgumentException">If old triangle set has lements with invadid Id or no Name</exception>
        /// <exception cref="ArgumentNullException">If old triangle is Null</exception>
        void ComputeTriangleBath(IEnumerable<HalpFormatData> oldTriangSet, out IEnumerable<HalpFormatData> newTriangSet);
    }
}
