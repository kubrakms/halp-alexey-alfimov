﻿// -----------------------------------------------------------------------
// <copyright file="HalpFormatData.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.Halp.Modules.CalcModule.Models
{
    using geometry;
    using System.Runtime.Serialization;

   
    [DataContract]
    public class HalpFormatData
    {
        [DataMember(Order = 1)]
        public long Id { get; set; }

        [DataMember(Order = 2)]
        public string Name { get; set; }

        [DataMember(Order = 3)]
        public Gm.Point V1 { get; set; }

        [DataMember(Order = 4)]
        public Gm.Point V2 { get; set; }

        [DataMember(Order = 5)]
        public Gm.Point V3 { get; set; }
    }
}
