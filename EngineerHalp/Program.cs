﻿
namespace SoftServe.EngineerHalp
{

    using System;
    using System.Resources;
    using System.Threading;

    using ViewModelResources = global::SoftServe.EngineerHalp.HalpResource;

    class Program
    {
        static void Main(string[] args)
        {
            using (var controller = new InputController(Thread.CurrentThread.CurrentUICulture))
            {
                (controller as InputController).Initialize();
                var resManager = new ResourceManager(typeof(ViewModelResources));
                Console.WriteLine(controller.GetProgramHelp());

              

                while (!controller.IsDown)
                {
                    var command = controller.GetCommand(resManager.GetString("PromtString", Thread.CurrentThread.CurrentUICulture));

                    if (string.Compare(command.Name, "Compute", true) == 0)
                    {
                        continue;
                    }

                    Console.WriteLine(controller.GetProgramHelp());
                }
                Console.WriteLine(resManager.GetString("FinalMessage", Thread.CurrentThread.CurrentUICulture));
                Console.ReadKey();
            }
        }
    }
}
