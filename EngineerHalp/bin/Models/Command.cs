﻿// -----------------------------------------------------------------------
// <copyright file="Command.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class Command
    {
        public string Name { get; set; }

        public object[] Params { get; set; }
    }
}
