﻿// -----------------------------------------------------------------------
// <copyright file="InputController.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace SoftServe.EngineerHalp
{

    using System;
    using System.Globalization;
    using System.Resources;
    using SoftServe.EngineerHalp.Models;
    
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    internal class InputController : IInputContract
    {
        public InputController(CultureInfo culture)
        {
            if (culture != default(CultureInfo))
                mCulture = culture;
        }

        public void Initialize()
        {
            mResManager = new ResourceManager(typeof(global::SoftServe.EngineerHalp.HalpResource));
        }

        public string InputFolder
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public string OutputFolder
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public bool IsDown
        {
            get;
            private set;
        }

        public event EventHandler OnInputFolderChanged;

        public geometry.Gm.Point GetPoint(string promt)
        {
            throw new NotImplementedException();
        }

        public Command GetCommand(string promt)
        {
            Console.Write(promt);
            var line = Console.ReadLine();

            var command = new Command() { Name = line };
            IsDown = (string.Compare(command.Name, mResManager.GetString("ExitName", mCulture), true) == 0);

            return command;
        }

        public void PrintPoint(geometry.Gm.Point point)
        {
            throw new NotImplementedException();
        }

        public void SaveFile(System.IO.Stream stream, string path)
        {
            throw new NotImplementedException();
        }

        public System.IO.Stream LoadFile(string path)
        {
            throw new NotImplementedException();
        }

        public string GetProgramHelp()
        {
            return mResManager.GetString("HelpString", mCulture);
        }

        public string GetVersion()
        {
            throw new NotImplementedException();
        }


        public void Dispose()
        {
            // throw new NotImplementedException();
        }


        private readonly CultureInfo mCulture = new CultureInfo("en-US");

        private ResourceManager mResManager = default(ResourceManager);
    }
}
